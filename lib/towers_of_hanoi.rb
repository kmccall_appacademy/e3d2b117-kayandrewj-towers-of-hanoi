class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1],[], []]
  end

  def play
    puts render
    puts "Which tower would you like to take a disc from? (1, 2, 3)\n"
    origin = gets.chomp.to_i - 1 
    puts "Where would you like to put this disc?\n"
    destination = gets.chomp.to_i - 1
    move(origin, destination)
    if won?
      puts "You won!"
      puts render
      raise "quitting" # This is a terrible way to quit something
    end
    play

  end

  def move(from_tower, to_tower)
    if !valid_move?(from_tower, to_tower)
      puts "Invalid Move! The disc on top must be smaller than the one underneath\n"
    else
      towers[to_tower] << towers[from_tower].pop
    end
  end

  def valid_move?(from_tower, to_tower)
    from = towers[from_tower]
    to = towers[to_tower]
    return false if from.empty?
    return false if !to.empty? && from.last > to.last
    return true
  end

  def won?
    towers[1].length == 3 || towers[2].length == 3
  end

  DISC_RENDERS = { #Something I'd like to implement eventually
    0 => "(|||||)",
    1 => "(|||)",
    2 => "(|)"
  }


  def render
    top_row = towers.map { |e| e.length >= 3 ? "(|||||)" : "       "  }
    mid_row = towers.map { |e| e.length >= 2 ? "(|||||)" : "       "  }
    bottom_row = towers.map { |e| e.length >= 1 ? "(|||||)" : "       "  }
    "#{top_row.join(" ")}\n#{mid_row.join(" ")}\n#{bottom_row.join(" ")}"
  end

end

TowersOfHanoi.new.play


# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
